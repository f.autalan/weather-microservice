import * as Joi from 'joi';

export const JoiValidationsSchema = Joi.object({
  API_IP_URL: Joi.string().required(),
  API_IP_TIMEOUT: Joi.number().default(30000),
  API_WEATHER_URL: Joi.string().required(),
  API_WEATHER_TIMEOUT: Joi.number().default(30000),
  API_WEATHER_API_KEY: Joi.string().required(),
});
