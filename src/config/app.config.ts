export const EnvConfiguration = () => ({
  apiIpUrl: process.env.API_IP_URL,
  apiIpTimeout: +process.env.API_IP_TIMEOUT,
  apiWeatherUrl: process.env.API_WEATHER_URL,
  apiWeatherTimeout: +process.env.API_WEATHER_TIMEOUT,
  apiWeatherKey: process.env.API_WEATHER_API_KEY,
});
