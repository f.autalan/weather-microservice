import { Controller, Get, Query } from '@nestjs/common';
import { FindForecastQueryDto, FindWeatherQueryDto } from './dto';
import { WeatherService } from './weather.service';

@Controller('weather')
export class WeatherController {
  constructor(private readonly weatherService: WeatherService) {}

  @Get('/current')
  findCurrentWeather(@Query() findWeatherQueryDto: FindWeatherQueryDto) {
    return this.weatherService.findCurrentWeather(findWeatherQueryDto);
  }

  @Get('/forecast')
  findForecast(@Query() findForecastQueryDto: FindForecastQueryDto) {
    return this.weatherService.findForecast(findForecastQueryDto);
  }
}
