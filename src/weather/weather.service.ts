import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ErrorHttp } from '../util/error';
import { LocationService } from '../location/location.service';
import {
  FindForecastQueryDto,
  FindWeatherQueryDto,
  ForecastResponseDto,
  WeatherResponseDTO,
} from './dto';

@Injectable()
export class WeatherService {
  private baseUrl: string;
  private apiKey: string;

  constructor(
    private readonly httpService: HttpService,
    private readonly locationService: LocationService,
    private readonly configService: ConfigService,
  ) {
    this.baseUrl = configService.get<string>('apiWeatherUrl');
    this.apiKey = configService.get<string>('apiWeatherKey');
  }

  private async getWeather(city: string): Promise<WeatherResponseDTO> {
    try {
      const { data } = await this.httpService.axiosRef.get<WeatherResponseDTO>(
        `${this.baseUrl}/weather?q=${city}&appid=${this.apiKey}`,
      );
      return data;
    } catch (error) {
      throw new ErrorHttp(error);
    }
  }

  private async getForecast(
    city: string,
    cnt = 40,
  ): Promise<ForecastResponseDto> {
    try {
      const { data } = await this.httpService.axiosRef.get<ForecastResponseDto>(
        `${this.baseUrl}/forecast?q=${city}&cnt=${cnt}&appid=${this.apiKey}`,
      );
      return data;
    } catch (error) {
      throw new ErrorHttp(error);
    }
  }

  private async getCity(findWeatherQueryDto: FindWeatherQueryDto) {
    let city = findWeatherQueryDto.city;
    if (!city) {
      const currentLocation = await this.locationService.findLocation();
      city = currentLocation.city;
    }
    return city;
  }

  async findCurrentWeather(
    findWeatherQueryDto: FindWeatherQueryDto,
  ): Promise<WeatherResponseDTO> {
    const city = await this.getCity(findWeatherQueryDto);
    const weather = await this.getWeather(city);
    return weather;
  }

  async findForecast(
    findForecastQueryDto: FindForecastQueryDto,
  ): Promise<ForecastResponseDto> {
    const city = await this.getCity(findForecastQueryDto);
    const timestamp = findForecastQueryDto.days
      ? (24 / 3) * findForecastQueryDto.days
      : 40;
    const forest = await this.getForecast(city, timestamp);
    return forest;
  }
}
