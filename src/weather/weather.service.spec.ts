import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { LocationResponseDto } from '../location/dto/location-response.dto';
import { LocationService } from '../location/location.service';
import {
  FindForecastQueryDto,
  FindWeatherQueryDto,
  ForecastResponseDto,
  WeatherResponseDTO,
} from './dto';
import { WeatherService } from './weather.service';
import { ErrorHttp } from '../util/error';

describe('WeatherService', () => {
  let service: WeatherService;

  const location: LocationResponseDto = {
    status: 'success',
    country: 'Argentina',
    countryCode: 'AR',
    region: 'B',
    regionName: 'undefined',
    city: 'Avellaneda',
    zip: '1000',
    lat: -34.8866,
    lon: -58.3834,
    timezone: 'America/Argentina/Buenos_Aires',
    isp: 'undefined',
    org: 'undefined',
    as: 'undefined',
    query: 'undefined',
  };

  const expected: WeatherResponseDTO = {
    coord: {
      lon: -1,
      lat: -1,
    },
    weather: [
      {
        id: 1,
        main: 'Clear',
        description: 'clear sky',
        icon: '01d',
      },
    ],
    base: 'stations',
    main: {
      temp: 302.24,
      feels_like: 301.85,
      temp_min: 301.46,
      temp_max: 303.1,
      pressure: 1008,
      humidity: 40,
    },
    visibility: 1,
    wind: {
      speed: 7.15,
      deg: 0,
      gust: 9.83,
    },
    clouds: {
      all: 0,
    },
    dt: 1,
    sys: {
      type: 2,
      id: 1,
      country: 'AR',
      sunrise: 1,
      sunset: 1,
    },
    timezone: -10800,
    id: 1,
    name: 'Avellaneda',
    cod: 1,
  };

  const findWeatherQueryDto: FindWeatherQueryDto = {
    city: 'Avellaneda',
  };

  const findForecastQueryDto: FindForecastQueryDto = {
    days: 5,
    city: 'Avellaneda',
  };

  const axiosError = {
    config: {},
    code: '',
    request: '',
    response: { data: {} },
    isAxiosError: true,
    toJSON: undefined,
    name: '',
    message: '',
    stack: '',
  };
  const expedtedForecast: ForecastResponseDto = {
    cod: '200',
    message: 0,
    cnt: 1,
    list: [
      {
        dt: 1,
        main: {
          temp: 1,
          feels_like: 1,
          temp_min: 1,
          temp_max: 301.73,
          pressure: 1,
          sea_level: 1,
          grnd_level: 1003,
          humidity: 45,
          temp_kf: 2.1,
        },
        weather: [
          {
            id: 800,
            main: 'Clear',
            description: 'clear sky',
            icon: '01d',
          },
        ],
        clouds: {
          all: 0,
        },
        wind: {
          speed: 3.94,
          deg: 14,
          gust: 7.42,
        },
        visibility: 1,
        pop: 0,
        sys: {
          pod: 'd',
        },
        dt_txt: '2023-03-28 21:00:00',
      },
    ],
    city: {
      id: 3433762,
      name: 'Glew',
      coord: {
        lat: -1,
        lon: -1,
      },
      country: 'AR',
      population: 0,
      timezone: -1,
      sunrise: 1,
      sunset: 1,
    },
  };
  const mockHttpService = {
    axiosRef: {},
  };
  const mockLocationService = {
    findLocation: jest.fn(() => {
      return { data: location };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [WeatherService, LocationService, HttpService, ConfigService],
    })
      .overrideProvider(LocationService)
      .useValue(mockLocationService)
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile();
    service = module.get<WeatherService>(WeatherService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findCurrentWeather', () => {
    it('sshould return an object of weatherResponseDto', async () => {
      mockHttpService.axiosRef = {
        get: jest.fn(() => {
          return { data: expected };
        }),
      };
      const received = await service.findCurrentWeather(findWeatherQueryDto);
      expect(received).toMatchObject(expected);
    });
    it('should return an Error BadRequestException', async () => {
      mockHttpService.axiosRef = {
        get: jest.fn(() => {
          throw axiosError;
        }),
      };

      try {
        await service.findCurrentWeather(findWeatherQueryDto);
      } catch (error) {
        expect(error).toBeInstanceOf(ErrorHttp);
      }
    });
  });

  describe('findForecast', () => {
    it('sshould return an object of weatherResponseDto', async () => {
      mockHttpService.axiosRef = {
        get: jest.fn(() => {
          return { data: expedtedForecast };
        }),
      };
      const received = await service.findForecast(findForecastQueryDto);
      expect(received).toMatchObject(expedtedForecast);
    });
    it('should return an Error BadRequestException', async () => {
      mockHttpService.axiosRef = {
        get: jest.fn(() => {
          throw axiosError;
        }),
      };

      try {
        await service.findCurrentWeather(findWeatherQueryDto);
      } catch (error) {
        expect(error).toBeInstanceOf(ErrorHttp);
      }
    });
  });
});
