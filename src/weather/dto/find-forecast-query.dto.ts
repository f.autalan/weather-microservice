import { IsOptional, IsNumber } from 'class-validator';
import { FindWeatherQueryDto } from './find-weather-query.dto';

export class FindForecastQueryDto extends FindWeatherQueryDto {
  @IsNumber()
  @IsOptional()
  days: number;
}
