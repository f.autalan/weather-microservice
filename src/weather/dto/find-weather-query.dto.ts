import { IsOptional, IsString, Length } from 'class-validator';
export class FindWeatherQueryDto {
  @IsString()
  @Length(4)
  @IsOptional()
  city?: string;
}
