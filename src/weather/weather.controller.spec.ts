import { Test, TestingModule } from '@nestjs/testing';
import { LocationService } from '../location/location.service';
import { FindWeatherQueryDto, WeatherResponseDTO } from './dto';
import { WeatherController } from './weather.controller';
import { WeatherService } from './weather.service';

describe('WeatherController', () => {
  let controller: WeatherController;
  const expected: WeatherResponseDTO = {
    coord: {
      lon: -1,
      lat: -1,
    },
    weather: [
      {
        id: 1,
        main: 'Clear',
        description: 'clear sky',
        icon: '01d',
      },
    ],
    base: 'stations',
    main: {
      temp: 302.24,
      feels_like: 301.85,
      temp_min: 301.46,
      temp_max: 303.1,
      pressure: 1008,
      humidity: 40,
    },
    visibility: 1,
    wind: {
      speed: 7.15,
      deg: 0,
      gust: 9.83,
    },
    clouds: {
      all: 0,
    },
    dt: 1,
    sys: {
      type: 2,
      id: 1,
      country: 'AR',
      sunrise: 1,
      sunset: 1,
    },
    timezone: -10800,
    id: 1,
    name: 'Avellaneda',
    cod: 1,
  };
  const mockWetherService = {
    findCurrentWeather: jest.fn(() => {
      return expected;
    }),
  };
  const mockLocationService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WeatherController],
      providers: [WeatherService, LocationService],
    })
      .overrideProvider(WeatherService)
      .useValue(mockWetherService)
      .overrideProvider(LocationService)
      .useValue(mockLocationService)
      .compile();

    controller = module.get<WeatherController>(WeatherController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('should return an object of weatherResponseDto', async () => {
    const findWeatherQueryDto: FindWeatherQueryDto = {
      city: 'Avellaneda',
    };
    const received = await controller.findCurrentWeather(findWeatherQueryDto);
    expect(received).toMatchObject(expected);
  });
});
