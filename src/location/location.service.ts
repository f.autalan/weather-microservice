import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { LocationStatus } from './constant/location-status';
import { LocationResponseDto } from './dto/location-response.dto';

@Injectable()
export class LocationService {
  private baseUrl: string;

  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {
    this.baseUrl = configService.get<string>('apiIpUrl');
  }
  async findLocation(): Promise<LocationResponseDto> {
    const { data } = await this.httpService.axiosRef.get<LocationResponseDto>(
      `${this.baseUrl}/json`,
    );
    if (data.status === LocationStatus.FAIL) {
      throw new BadRequestException();
    }
    return data;
  }
}
