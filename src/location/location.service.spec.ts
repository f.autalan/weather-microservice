import { HttpService } from '@nestjs/axios';
import { BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { LocationResponseDto } from './dto/location-response.dto';
import { LocationService } from './location.service';

describe('LocationService', () => {
  let service: LocationService;

  const expected: LocationResponseDto = {
    status: 'success',
    country: 'Argentina',
    countryCode: 'AR',
    region: 'B',
    regionName: 'undefined',
    city: 'undefined',
    zip: '1000',
    lat: -34.8866,
    lon: -58.3834,
    timezone: 'America/Argentina/Buenos_Aires',
    isp: 'undefined',
    org: 'undefined',
    as: 'undefined',
    query: 'undefined',
  };

  const mockHttpService = {
    axiosRef: {
      get: jest.fn(() => {
        return { data: expected };
      }),
    },
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LocationService, HttpService, ConfigService],
    })
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile();

    service = module.get<LocationService>(LocationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return an object of locationResponseDto', async () => {
    const received = await service.findLocation();
    expect(received).toMatchObject(expected);
    expect(received.query).toEqual(expected.query);
  });

  it('should return an Error BadRequestException', async () => {
    const data: LocationResponseDto = {
      status: 'fail',
      query: 'undefined',
    };

    mockHttpService['axiosRef'] = {
      get: jest.fn(() => {
        return { data };
      }),
    };
    expect(async () => {
      await service.findLocation();
    }).rejects.toThrow(BadRequestException);
  });
});
