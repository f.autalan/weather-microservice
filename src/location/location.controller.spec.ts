import { Test, TestingModule } from '@nestjs/testing';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';
import { LocationResponseDto } from './dto/location-response.dto';

describe('LocationController', () => {
  let controller: LocationController;

  const expected: LocationResponseDto = {
    status: 'success',
    country: 'Argentina',
    countryCode: 'AR',
    region: 'B',
    regionName: 'undefined',
    city: 'undefined',
    zip: '1000',
    lat: -34.8866,
    lon: -58.3834,
    timezone: 'America/Argentina/Buenos_Aires',
    isp: 'undefined',
    org: 'undefined',
    as: 'undefined',
    query: 'undefined',
  };

  const mockService = {
    findLocation: jest.fn(() => {
      return expected;
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LocationController],
      providers: [LocationService],
    })
      .overrideProvider(LocationService)
      .useValue(mockService)
      .compile();

    controller = module.get<LocationController>(LocationController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should return an object of locationResponseDto', async () => {
    const received = await controller.findLocation();
    expect(received).toMatchObject(expected);
  });
});
