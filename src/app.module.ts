import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppService } from './app.service';
import { LocationModule } from './location/location.module';
import { EnvConfiguration } from './config/app.config';
import { JoiValidationsSchema } from './config/joi.validation';
import { WeatherModule } from './weather/weather.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [EnvConfiguration],
      validationSchema: JoiValidationsSchema,
    }),
    LocationModule,
    WeatherModule,
  ],
  providers: [AppService],
})
export class AppModule {}
