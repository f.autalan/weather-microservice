import {
  ExceptionFilter,
  ArgumentsHost,
  HttpStatus,
  Catch,
} from '@nestjs/common';
import { Response } from 'express';
import { ErrorHttp } from './error';

@Catch(ErrorHttp)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: ErrorHttp, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    console.log(exception);
    let body: { statusCode: number; message: string } = {
      statusCode: 500,
      message: 'Internal server error',
    };
    if (exception.code === 'ECONNABORTED') {
      body = {
        statusCode: 408,
        message: 'Request timeout',
      };
    }

    if (exception.response) {
      const status = exception.response.status;
      switch (status) {
        case HttpStatus.UNAUTHORIZED:
          body = {
            statusCode: status,
            message: 'Unauthorized',
          };
          break;
        case HttpStatus.FORBIDDEN:
          body = {
            statusCode: status,
            message: 'Forbiden',
          };
          break;
        case HttpStatus.BAD_REQUEST:
          body = {
            statusCode: status,
            message: 'Bad request',
          };
          break;
        case HttpStatus.NOT_FOUND:
          body = {
            statusCode: status,
            message: 'Not Fount',
          };
          break;
        case HttpStatus.REQUEST_TIMEOUT:
          body = {
            statusCode: status,
            message: 'Request timeout',
          };
          break;
        default:
          body = {
            statusCode: 500,
            message: 'Internal server error',
          };
      }
    }

    response.status(body.statusCode).json(body);
  }
}
