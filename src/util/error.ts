import { AxiosError, AxiosResponse } from 'axios';
export class ErrorHttp implements AxiosError {
  code?: string;
  request?: any;
  response?: AxiosResponse;
  isAxiosError: boolean;
  toJSON: () => object;
  name: string;
  message: string;
  stack?: string;

  constructor(error: AxiosError) {
    this.code = error.code;
    this.request = error.request;
    this.response = error.response;
    this.isAxiosError = error.isAxiosError;
    this.toJSON = error.toJSON;
    this.name = error.name;
    this.message = error.message;
    this.stack = error.stack;
  }
}
